package com.ssims.service.system;

import com.ssims.dao.system.SystemDAO;
import com.ssims.security.entity.UserEntity;
import com.ssims.security.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/6/10.
 */
@Transactional
@Service("systemService")
public class SystemService extends BaseService {
    @Autowired
    private SystemDAO systemDAO;

    public List<UserEntity> queryAllUser() {
        return systemDAO.query();
    }

    public String addUser(UserEntity entity) {
        return null;
    }

    public boolean deleteUserById(String uid) {
        return false;
    }
}
