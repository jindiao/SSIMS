package com.ssims.service.client;

import com.ssims.dao.client.GoodsInfoDAO;
import com.ssims.model.entity.GoodsInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/5/8.
 */
@Transactional
@Service("goodsInfoService")
public class GoodsInfoService {
    @Autowired
    private GoodsInfoDAO goodsInfoDAO;

    public List<GoodsInfoEntity> queryByParams() {
        return goodsInfoDAO.query();
    }

    public List<GoodsInfoEntity> saveAndRefresh(GoodsInfoEntity entity) {
        goodsInfoDAO.insert(entity);
        return goodsInfoDAO.query();
    }

    public GoodsInfoEntity updateCusInfo(GoodsInfoEntity entity) {
        boolean success = goodsInfoDAO.update(entity);
        if (success) {
            return goodsInfoDAO.queryById(entity.getId());
        }
        return null;
    }

    public boolean delete(String id) {
        return goodsInfoDAO.delete(id);
    }
}
