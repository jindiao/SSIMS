package com.ssims.dao.client;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.CusInfoEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/5/7.
 */
@Repository("cusInfoDAO")
public class CusInfoDAO extends BaseDAO {

    public List<CusInfoEntity> query() {
        return sqlSession.selectList("CusInfoMapper.select");
    }


    public int insert(CusInfoEntity entity) {
        return sqlSession.insert("CusInfoMapper.insert", entity);
    }

    public boolean delete(Integer id) {
        int count = sqlSession.delete("CusInfoMapper.deleteById", id);
        if (count > 0) return true;
        return false;
    }

    public boolean update(CusInfoEntity entity) {
        int count = sqlSession.update("CusInfoMapper.update", entity);
        if (count > 0) return true;
        return false;
    }

    public CusInfoEntity queryById(Integer id) {
        return sqlSession.selectOne("CusInfoMapper.selectOneById", id);
    }
}
