package com.ssims.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * DAO基类注入mybaits的相关类
 * Created by coliza on 2014/5/1.
 */
public abstract class BaseDAO {
    @Autowired
    protected SqlSessionTemplate sqlSession;
}
