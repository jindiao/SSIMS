package com.ssims.security.dao;

import com.ssims.security.entity.SecurityUser;
import org.springframework.stereotype.Repository;

/**
 * Created by coliza on 2015/9/2.
 */
@Repository("securityUserDAO")
public class SecurityUserDAO extends BaseDAO {
    public SecurityUser findByUsername(String username){
        return sqlSession.selectOne("ISecurityUserMapper.select",username);
    }
}
