package com.ssims.security.dao;

import com.ssims.security.entity.SecurityPermission;
import org.springframework.stereotype.Repository;

/**
 * @description 权限DAO
 * Created by coliza on 2015/9/16.
 */
@Repository("securityPermissionDAO")
public class SecurityPermissionDAO extends BaseDAO {
    public SecurityPermission findSinglePermission(String permission) {
        return sqlSession.selectOne("ISecurityPermissionMapper.querySinglePermission",permission);
    }
}
