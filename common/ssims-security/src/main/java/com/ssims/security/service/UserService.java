package com.ssims.security.service;

import com.ssims.security.dao.SecurityPermissionDAO;
import com.ssims.security.dao.SecurityRoleDAO;
import com.ssims.security.dao.SecurityUserDAO;
import com.ssims.security.entity.SecurityPermission;
import com.ssims.security.entity.SecurityRole;
import com.ssims.security.entity.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by coliza on 2015/9/1.
 */
@Service("userService")
public class UserService {
    @Autowired
    private SecurityUserDAO securityUserDAO;
    @Autowired
    private SecurityRoleDAO securityRoleDAO;
    @Autowired
    private SecurityPermissionDAO securityPermissionDAO;

    public SecurityUser findByUsername(String username) {
        return securityUserDAO.findByUsername(username);
    }

    public SecurityRole findRole(String role) {
        return securityRoleDAO.findByRole(role);
    }

    public SecurityPermission findPermission(String permission) {
        return securityPermissionDAO.findSinglePermission(permission);
    }
}
