package com.ssims.security.entity;

import java.util.HashSet;

/**
 * Created by coliza on 2015/9/1.
 */
public class SecurityRole {
    private int id;
    private String role;
    private String description;
    private HashSet<SecurityPermission> permissions;
    private boolean available;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HashSet<SecurityPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(HashSet<SecurityPermission> permissions) {
        this.permissions = permissions;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
