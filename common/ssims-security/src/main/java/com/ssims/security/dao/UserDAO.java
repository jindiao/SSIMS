package com.ssims.security.dao;

import com.ssims.security.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

/**
 * 用户DAO，保存用户信息
 * Created by coliza on 2014/5/1.
 */
@Repository("userDAO")
public class UserDAO extends BaseDAO{

    public UserEntity getUserById(String uid) {
        return sqlSession.selectOne("UserMapper.selectOneById", uid);
    }

    public UserEntity getUserByUsername(String username) {
        return sqlSession.selectOne("UserMapper.selectOneByUsername", username);
    }

    public List<UserEntity> queryAllUser() throws SQLException {
        return sqlSession.selectList("UserMapper.selectAll");
    }
}
