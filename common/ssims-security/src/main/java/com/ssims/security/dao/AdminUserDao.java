package com.ssims.security.dao;

import com.ssims.security.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by coliza on 2015/8/24.
 */
@Repository("adminUserDao")
public class AdminUserDao extends BaseDAO{
    public UserEntity getUserByUsername(String username) {
        return sqlSession.selectOne("AdminUserMapper.selectOneByUsername", username);
    }

    public List<UserEntity> queryUsersByCondition(Map params) {
        return sqlSession.selectList("AdminUserMapper.queryUsersByConditions", params);
    }
}
