package com.ssims.security.entity;

/**
 * 用户实体类
 * Created by coliza on 2014/5/1.
 */
public class UserEntity extends SecurityUser {

    public UserEntity() {
        super();
    }

    public UserEntity(String username, char[] password) {
        super(username, password);
    }
}
