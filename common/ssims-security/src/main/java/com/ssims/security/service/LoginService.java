package com.ssims.security.service;

import com.ssims.security.dao.SecurityUserDAO;
import com.ssims.security.entity.SecurityUser;
import com.ssims.security.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

/**
 * 登入登出服务实现类
 * Created by coliza on 2014/5/1.
 */
@Transactional
@Service("loginService")
public class LoginService extends BaseService{
    @Autowired
    private SecurityUserDAO securityUserDAO;

    public boolean loginIn(HttpServletRequest request, UserEntity entity) {
        return false;
    }

    public boolean loginOn(HttpServletRequest request, UserEntity entity) {
        if (entity == null || null == entity.getUsername() || null == entity.getPassword()) {
            return false;
        }
        SecurityUser sourceUser = securityUserDAO.findByUsername(entity.getUsername());
        if (null == sourceUser) {
            return false;
        }
        if (sourceUser.getPassword() == null) {
            return false;
        }
        if (entity.getPassword() == null) {
            return false;
        }
        if (!sourceUser.getPassword().equals(entity.getPassword())) {
            return false;
        }
        return true;
    }
}
