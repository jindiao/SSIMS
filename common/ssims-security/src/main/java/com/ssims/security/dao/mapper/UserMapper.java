package com.ssims.security.dao.mapper;


import com.ssims.security.entity.UserEntity;

import java.util.List;

/**
 * Created by coliza on 2014/5/3.
 */
public interface UserMapper {
    public UserEntity selectOneByUsername(String username);

    public List<UserEntity> selectAll();
}
