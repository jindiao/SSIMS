package com.ssims.security.dao.mapper;

import com.ssims.security.entity.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * Created by coliza on 2015/8/24.
 */
public interface AdminUserMapper {
    //获取单个用户
    public UserEntity selectOneByUsername(String username);

    //按条件获取用户
    public List<UserEntity> queryUsersByConditions(Map params);
}
