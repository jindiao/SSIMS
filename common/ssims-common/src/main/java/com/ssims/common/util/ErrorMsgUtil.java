package com.ssims.common.util;

import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by coliza on 2014/5/1.
 */
public class ErrorMsgUtil {
    private static final String MSG = "msg";

    public static void decorateMsg(ModelAndView view, String msg) {
        view.addObject(MSG, msg);
    }

    public static void decorateMsg(HttpServletRequest request, String msg) {
        request.setAttribute(MSG, msg);
    }
}
