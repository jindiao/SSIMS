<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/5
  Time: 23:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<ul class="according">
    <li class="title group"><a class="title-label" href="javsscript:void(0);"
                               onclick="window.location.href = '${contextPath}/front/login/logOn';">首页</a></li>
    <shiro:hasRole name="user">
        <li class="title group">
            <div class="title-label">基础信息管理</div>
            <ul class="menu">
                <li><a href="${contextPath}/front/fim/cim?method=show">客户信息管理</a></li>
                <li><a href="${contextPath}/front/fim/gim?method=show">商品信息管理</a></li>
                <li><a href="${contextPath}/front/fim/sim?method=show">供应商信息管理</a></li>
            </ul>
        </li>
        <li class="title group">
            <div class="title-label">库存</div>
            <ul class="menu">
                <li><a href="${contextPath}/front/gsm/wh?method=show">库存管理</a></li>
                <li><a href="${contextPath}/front/gsm/ewh?method=show">出入库记录管理</a></li>
            </ul>
        </li>
        <li class="title group">
            <div class="title-label">商品销售</div>
            <ul class="menu">
                <li><a href="${contextPath}/front/gs/sm?method=show">销售管理</a></li>
            </ul>
        </li>
        <li class="title group">
            <div class="title-label">结账管理</div>
            <ul class="menu">
                <li><a href="${contextPath}/front/sc/scm?method=show">销售结账管理</a></li>
                <!--<li><a href="${contextPath}/sc/stcm?method=show">库存结账管理</a></li> -->
            </ul>
        </li>
        <li class="title group">
            <div class="title-label">统计信息</div>
            <ul class="menu">
                <li><a href="${contextPath}/front/sa/sapy?method=show">年销售额分析</a></li>
            </ul>
        </li>
    </shiro:hasRole>
</ul>
<script type="text/javascript">
    $(function () {
        $(".according").accordion({
            icons: {
                header: "collapse-icon_default.png",
                activeHeader: "collapse-icon_active.png"
            }
        });
        $(".according .group .menu li").mouseover(function (e) {
            e.stopPropagation();
            $(this).css({
                "backgroundColor": "#245293",
                "color": "#FFF"
            });
            $(this).find("a").css("color", "#FFF");
        });
        $(".according .group .menu li").mouseout(function (e) {
            $(this).css({
                "backgroundColor": "#FFF",
                "color": "#000"
            });
            $(this).find("a").css("color", "#000");
        });
        $(".according .group .menu").css({
            "height": "auto",
            "padding": "0px"
        });
    });
</script>