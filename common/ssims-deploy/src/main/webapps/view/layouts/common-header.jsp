<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/2
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<%
    String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html>
<head>
    <title>进销存管理系统</title>
    <script type="text/javascript">
        function getToday() {
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth();
            var day = today.getDate();
            var hour = today.getHours();
            var minute = today.getMinutes();
            var second = today.getSeconds();
            $("#jsDateToday").html("现在是北京时间 " + year + "年" + month + "月" + day + "日  " + hour + "时" + minute + "分" + second + "秒");
        }
        $(function () {
            setInterval("getToday()", 1000);
        });
    </script>
</head>
<body>
<div class="body">
    <div class="header">

        <shiro:hasRole name="admin">
            <div class="tool">
                <div class="fr"><a href="${contextPath}/admin/user?method=show">[管理系统]</a></div>
                <div><span id="jsDateToday"></span></div>

            </div>
        </shiro:hasRole>

        <img class="logo" src="${contextPath}/resources/images/logo.jpg"/>
    </div>
