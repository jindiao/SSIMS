<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/5
  Time: 23:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<ul class="according">
    <li class="title group"><a class="title-label" href="javsscript:void(0);"
                               onclick="window.location.href = '${contextPath}/front/login/logOn';">首页</a></li>
    <shiro:hasRole name="admin">
        <li class="title group">
            <div class="title-label">系统管理</div>
            <ul class="menu">
                <li><a href="${contextPath}/system/account/view">密码变更</a></li>
                <li><a href="${contextPath}/system/account/admin/password">用户管理</a></li>
            </ul>
        </li>
        <li class="title group">
            <div class="title-label">用户管理</div>
            <ul class="menu">
                <li><a href="${contextPath}/admin/user/manage">用户管理</a></li>
            </ul>
        </li>
    </shiro:hasRole>
</ul>
<script type="text/javascript">
    $(function () {
        $(".according").accordion({
            icons: {
                header: "collapse-icon_default.png",
                activeHeader: "collapse-icon_active.png"
            }
        });
        $(".according .group .menu li").mouseover(function (e) {
            e.stopPropagation();
            $(this).css({
                "backgroundColor": "#245293",
                "color": "#FFF"
            });
            $(this).find("a").css("color", "#FFF");
        });
        $(".according .group .menu li").mouseout(function (e) {
            $(this).css({
                "backgroundColor": "#FFF",
                "color": "#000"
            });
            $(this).find("a").css("color", "#000");
        });
        $(".according .group .menu").css({
            "height": "auto",
            "padding": "0px"
        });
    });
</script>