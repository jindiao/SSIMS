<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/4
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:insertAttribute name="common-header-taglib"></tiles:insertAttribute>
<tiles:insertAttribute name="common-header-css"></tiles:insertAttribute>
<tiles:insertAttribute name="common-header-scripts"></tiles:insertAttribute>
<tiles:insertAttribute name="common-header"></tiles:insertAttribute>
<div class="content">
    <div class="left">
        <tiles:insertAttribute name="left-menu"></tiles:insertAttribute>
    </div>
    <div class="right">
        <tiles:putAttribute name="url" value="/view/jsp/${url}.jsp"></tiles:putAttribute>
        <tiles:insertAttribute name="url"></tiles:insertAttribute>
    </div>
</div>
<div class="footer" align="center">
    <tiles:insertAttribute name="rights-footer"></tiles:insertAttribute>
</div>
</div>
</body>
</html>