<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/5
  Time: 23:13
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<div class="news">
    <div class="header">新闻公告</div>
    <div class="news-c">
        <div class="item">公司国庆放假通知！</div>
        <div class="item">年度销售额再创佳绩，向全体员工的一年的辛勤付出致敬！</div>
        <div class="item">12月12日，公司正式取得计算机系统集成2级资质</div>
        <div class="item">公司电商平台即将推出敬请期待！</div>
        <div class="item">我司联合附近商家面向全体员工的优惠活动</div>
        <div class="item">关于近期公司新版报销的流程规范的通知</div>
        <div class="item">今天是员工XXX的生日，让我们一起祝她生日快乐！</div>
        <div class="item">本周末公司将组织全体员工在奥森举办撕名牌活动！</div>
        <div class="item">公司今年将推一款全新产品！</div>
        <div class="item">公司通过关于产品部XXX利用职务之便收取回扣的处罚通知！</div>
        <div class="item">公司内部推荐系统正式上线！</div>
        <div class="item">近日有员工陷入诈骗陷阱，请全体同事注意保护财产安全！</div>
    </div>
</div>
