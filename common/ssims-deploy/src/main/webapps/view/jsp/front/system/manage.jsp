<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/6/2
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<div class="system">
    <div class="tool">
        <div class="row">
            <button onclick="showAddDialog()" value="添加"></button>
        </div>
    </div>
    <div class="table">
        <div class="thead">
            <div class="row">
                <div class="col">序号</div>
                <div class="col">账号</div>
                <div class="col">密码</div>
                <div class="col">操作</div>
            </div>
        </div>
        <div class="tbody">
            <c:forEach items="${records}" var="item" varStatus="index">
            <div class="row">
                <div class="col">${index.count+1}</div>
                <div class="col">${item.name}/div>
                    <div class="col">${item.password}</div>
                    <div class="col">
                        <a href="javascript:void(0);">删除</a>
                    </div>
                </div>
                </c:forEach>
            </div>
        </div>
    </div>
