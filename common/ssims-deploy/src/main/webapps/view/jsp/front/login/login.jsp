<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/1
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>网站登录</title>
    <script src="/resources/scripts/jquery-1.11.1.js"></script>
</head>
<body>
<table width="100%" height="451" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table width="509" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="142"><img src="/resources/images/login_top.jpg" width="509" height="142"/></td>
                </tr>
                <tr>
                    <td height="159" valign="top" background="/resources/images/login_bottom.jpg">
                        <table width="350" height="116" border="0" cellpadding="0" cellspacing="0">
                            <form:form modelAttribute="formBean" commandName="formBean" action="/front/login/logOn"
                                       onsubmit="check()">
                                <tr>
                                    <td align="center" colspan="3">
                                        <span><font color="red">${msg}</font></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="91">&nbsp;</td>
                                    <td width="80" height="46">用户名：</td>
                                    <td width="200"><form:input path="username"
                                                                cssStyle="background-color:#F5F5F5;height:30px;padding:3px 2px 3px 2px;border-top:1px solid #ddd;border-left:1px solid #ddd;border-right:1px solid #FFF;border-botton:1px solid #FFF;line-height:18px;width:160px;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td height="28">密&nbsp;&nbsp;码：</td>
                                    <td><form:password path="password"
                                                       cssStyle="background-color:#F5F5F5;height:30px;padding:3px 2px 3px 2px;border-top:1px solid #ddd;border-left:1px solid #ddd;border-right:1px solid #FFF;border-botton:1px solid #FFF;line-height:18px;width:160px;"/></td>
                                </tr>
                                <tr>
                                    <td height="37" colspan="3" align="center"
                                        style="padding-top:20px;padding-left:20px;">
                                        <input name="submit" type="submit" class="yui3-button" value="登 录"> &nbsp;
                                        <input name="reset" type="reset" class="yui3-button" value="取 消">
                                    </td>
                                </tr>
                            </form:form>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="fixPanel" style="width:40px;height:50px;position:fixed;left:-20px;top:300px;border:solid 1px #ff0000;">
    这是一个panel
</div>
</body>
</html>
<script type="text/javascript">
    $(function () {
        $("#fixPanel").hover(function () {
            $(this).css("left", "0px");
        });
    });
</script>