<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/6/2
  Time: 23:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="nav-bar">
    <ol class="breadcrumb">
        <li class="breadcrumb-head"><a href="#">首页</a></li>
        <li class="breadcrumb-item"><a href="#">结账管理</a></li>
        <li class="breadcrumb-item active"><a href="#">入库结账管理</a></li>
    </ol>
</div>
<div class="query-pane"></div>
<div class="records table-responsive">
    <div class="toolbar" align="right">
        <div class="insert-btn btn btn-primary" id="insert_import_btn">商品入库结账</div>
        <div class="insert-btn btn btn-primary" id="insert_return_btn">销售退货结账</div>
    </div>
    <table>
        <thead>
        <tr>
            <th>结款票号</th>
            <th>销售票号</th>
            <th>客户名称</th>
            <th>本次结款</th>
            <th>余额</th>
            <th>结款日期</th>
            <th>操作员</th>
            <th>经手人</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${records}" var="item" varStatus="index">
            <tr>
                <td>${item.name}</td>
                <td>${item.origin}</td>
                <td>${item.price}</td>
                <td>${item.amount}</td>
                <td>
                    <a href="#" onclick="">修改</a>&nbsp;
                    <a href="#" onclick="">删除</a>&nbsp;
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

