<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/28
  Time: 22:20
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<div class="content">
    <div class="nav-bar">
        <ol class="breadcrumb">
            <li class="breadcrumb-head"><a href="#">首页</a></li>
            <li class="breadcrumb-item"><a href="#">库存管理</a></li>
            <li class="breadcrumb-item active"><a href="#">库存信息管理</a></li>
        </ol>
    </div>
    <div class="query-pane"></div>
    <div class="records table-responsive">
        <div class="toolbar" align="right">
            <div class="insert-btn btn btn-primary" id="insert_import_btn">进货</div>
            <div class="insert-btn btn btn-primary" id="insert_return_btn">退货</div>
        </div>
        <table class="table table-striped table-bordered table-hover table-condensed" width="800">
            <thead>
            <tr>
                <th width="5%" align="center">序号</th>
                <th width="12%" align="center">商品名</th>
                <th width="12%" align="center">产地</th>
                <th width="12%" align="center">库存</th>
                <th width="12%" align="center">操作</th>
            </tr>
            </thead>
            <tbody class="table-hover">
            <c:forEach items="${records}" var="item" varStatus="index">
                <c:if test="${index.index%2 eq 0}">
                    <tr class="odd">
                        <td align="center">${index.index+1}</td>
                        <td>${item.name}</td>
                        <td>${item.origin}</td>
                        <td>${item.amount}</td>
                        <td align="center" valign="center">
                            <a href="#" onclick="">查看详细</a>&nbsp;
                            <a href="#" onclick="">删除</a>&nbsp;
                        </td>
                    </tr>
                </c:if>
                <c:if test="${index.index%2 != 0}">
                    <tr class="even">
                        <td align="center">${index.index+1}</td>
                        <td>${item.name}</td>
                        <td>${item.origin}</td>
                        <td>${item.amount}</td>
                        <td align="center" valign="center">
                            <a href="#" onclick="">查看详细</a>&nbsp;
                            <a href="#" onclick="">删除</a>&nbsp;
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</div>