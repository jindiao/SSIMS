package com.ssims.model.entity;

import java.util.Date;

/**
 * Created by coliza on 2014/5/29.
 */
public class WareHRecordEntity {
    private String id;
    private GoodsInfoEntity goodsInfo;
    private Integer amount;
    private Double price;
    private Double paymentDue;
    private Double payment;
    private Double paymentMade;
    private String operatorId;
    private String handlerId;
    private Date date;
    private String payType;
    private String type;
    private String whNo;

    public Double getPaymentMade() {
        return paymentMade;
    }

    public void setPaymentMade(Double paymentMade) {
        this.paymentMade = paymentMade;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GoodsInfoEntity getGoodsInfo() {
        return goodsInfo;
    }

    public void setGoodsInfo(GoodsInfoEntity goodsInfo) {
        this.goodsInfo = goodsInfo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(Double paymentDue) {
        this.paymentDue = paymentDue;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public Double getGetPaymentMade() {
        return paymentMade;
    }

    public void setGetPaymentMade(Double paymentMade) {
        this.paymentMade = paymentMade;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getHandlerId() {
        return handlerId;
    }

    public void setHandlerId(String handlerId) {
        this.handlerId = handlerId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWhNo() {
        return whNo;
    }

    public void setWhNo(String whNo) {
        this.whNo = whNo;
    }
}
