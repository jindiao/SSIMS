package com.ssims.controller.front;

import com.ssims.controller.BaseController;
import com.ssims.service.sale.SaleCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 * 销售结账controller
 */
@Controller("saleCheckManagerController")
@RequestMapping("/front/sc/scm")
public class SaleCheckManagerController extends BaseController {
    @Autowired
    private SaleCheckService saleCheckService;

    @RequestMapping(params = "method=show")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        List list = saleCheckService.queryByParams();
        request.setAttribute("records", list);
        return mainLayout("front/sc/scm", request, response);
    }
}
