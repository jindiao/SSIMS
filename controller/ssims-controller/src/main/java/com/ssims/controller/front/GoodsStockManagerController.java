package com.ssims.controller.front;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ssims.controller.BaseController;
import com.ssims.model.entity.GoodsStockInfoEntity;
import com.ssims.service.sale.GoodsStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 */
@Controller("goodsStockManagerController")
@RequestMapping("/front/gsm/wh")
public class GoodsStockManagerController extends BaseController {
    @Autowired
    private GoodsStockService goodsStockService;

    @RequestMapping(params = "method=show")
    public ModelAndView show(HttpServletRequest request, HttpServletResponse response) {
        List list = goodsStockService.queryByParams();
        request.setAttribute("records", list);
        return mainLayout("front/gsm/wh", request, response);
    }

    @RequestMapping(params = "method=insert")
    public
    @ResponseBody
    String insert(HttpServletRequest request, HttpServletResponse response, GoodsStockInfoEntity entity) {
        List list = goodsStockService.saveAndRefresh(entity);
        JSONArray listArray = new JSONArray();
        JSONObject obj = new JSONObject();
        if (null == list) {
            obj.put("items", null);
            return obj.toString();
        }
        listArray.addAll(list);
        obj.put("items", listArray);
        return obj.toJSONString();
    }

    @RequestMapping(params = "method=delete")
    public
    @ResponseBody
    String delete(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Integer id) {
        Boolean success = goodsStockService.delete(id);
        JSONObject obj = new JSONObject();
        obj.put("success", success);
        return obj.toString();
    }
}
