package com.ssims.controller.front;

import com.ssims.controller.BaseController;
import com.ssims.security.entity.SecurityUser;
import com.ssims.security.service.LoginService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录控制器，控制普通用户的登入和登出
 * Created by coliza on 2014/5/1.
 */
@Controller("loginController")
@RequestMapping("/front/login")
public class LoginController extends BaseController {
    //日志记录器
    private static final Logger log = LogManager.getLogger(LoginController.class);
    @Autowired
    private LoginService loginService;

    @RequestMapping("/logIn")
    public ModelAndView logIn(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("formBean") SecurityUser userEntity) {
        //导向主页View
        ModelAndView view = new ModelAndView(new InternalResourceView("/jsp/front/main.jsp"));

        return view;
    }

    @RequestMapping("/toLogin")
    public ModelAndView toLogin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("formBean") SecurityUser userEntity) {
        return toLogOnPage(request, response);
    }

    @RequestMapping("/logOn")
    public ModelAndView logOn(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("formBean") SecurityUser userEntity) {
        System.out.println("username:admin, password:" + userEntity.getPassword());
        try {
            if (!StringUtils.isEmpty(userEntity.getUsername()) && !StringUtils.isEmpty(userEntity.getPassword().toString())) {
                Subject user = SecurityUtils.getSubject();
                user.login(userEntity);
                String userID = (String) user.getPrincipal();
            }
        } catch (UnknownAccountException e1) {
            log.error("认证失败！用户账号不存在");
            return toLogOnPage(request, response);
        } catch (IncorrectCredentialsException e2) {
            log.error("认证失败！用户密码不正确");
            return toLogOnPage(request, response);
        } catch (LockedAccountException e3) {
            log.error("认证失败！用户账号被锁定");
            return toLogOnPage(request, response);
        } catch (AuthenticationException e4) {
            log.error("认证失败！用户：" + userEntity.getUsername() + " 密码：" + userEntity.getPassword().toString());
            return toLogOnPage(request, response);
        }

        return mainLayout("front/main", request, response);
    }

    @RequestMapping("/logOut")
    public ModelAndView logOut(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("formBean") SecurityUser userEntity) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return toLogOnPage(request, response);
    }
}
