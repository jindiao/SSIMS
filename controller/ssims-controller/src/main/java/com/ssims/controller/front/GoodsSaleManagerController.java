package com.ssims.controller.front;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ssims.controller.BaseController;
import com.ssims.model.entity.GoodsSaleEntity;
import com.ssims.service.client.CusInfoService;
import com.ssims.service.client.GoodsInfoService;
import com.ssims.service.sale.GoodsSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 */
@Controller("goodsSaleManagerController")
@RequestMapping("/front/gs/sm")
public class GoodsSaleManagerController extends BaseController {
    @Autowired
    private GoodsSaleService goodsSaleService;
    @Autowired
    private GoodsInfoService goodsInfoService;
    @Autowired
    private CusInfoService cusInfoService;

    @RequestMapping(params = "method=show")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        List list = goodsSaleService.queryByParams();
        request.setAttribute("records", list);
        //获取所有商品
        List goodsList = goodsInfoService.queryByParams();
        request.setAttribute("goods", goodsList);
        //获取所有客户
        List customerList = cusInfoService.queryByParams();
        request.setAttribute("customers", customerList);
        return mainLayout("front/gs/sm", request, response);
    }
    @RequestMapping(params = "method=insert")
    public
    @ResponseBody
    String insert(HttpServletRequest request, HttpServletResponse response, GoodsSaleEntity entity) {
        int newId = goodsSaleService.saveAndRefresh(entity);
        JSONArray listArray = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put("newId", newId);
        return obj.toString();
    }

    @RequestMapping(params = "method=delete")
    public
    @ResponseBody
    String delete(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") String id) {
        Boolean success = goodsSaleService.delete(id);
        JSONObject obj = new JSONObject();
        obj.put("success", success);
        return obj.toString();
    }
}
